import {Component, OnInit, Inject, Output, EventEmitter, Input} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from "@angular/router";

import {NbDialogService} from "@nebular/theme";
import {MatSelectChange} from '@angular/material';
import Swal from "sweetalert2";
import {ModelDataAffecterTestCanidiat} from '../data/model-data-affecterTest';
import {TestService} from '../../services/test.service';
import {CandidatService} from '../../services/candidat.service';
import { DateAdapter, MatDatepickerInputEvent } from '@angular/material';
import {ReunionService} from '../../services/reunion.service';





@Component({
  selector: 'app-add-reunion',
  templateUrl: './add-reunion.component.html',
  styleUrls: ['./add-reunion.component.scss']
})
export class AddReunionComponent implements OnInit {
addMettingForm:FormGroup;
  idCandiat:any;
  listCandidat:[]=[];
  showSpinners:boolean;
  stepHour:number=1;
  stepMinute:number=1;
  disabled:boolean=false;
  enableMeridian:boolean=true;
  hideTime:boolean=false;
  disableMinute:boolean=true;
  private dateTimeLocal: Date;
  private dateTimeLocalend:Date;
  touchUi:boolean=true;





  constructor(public dialogRef: MatDialogRef<AddReunionComponent>,private adapter : DateAdapter<any>,
              private  fb: FormBuilder,private  candidatService:CandidatService,private reunionService:ReunionService,private router:Router)
  {

    this.candidatService.getAllCandidat().toPromise().then(reponse=>{
      this.listCandidat=JSON.parse(JSON.stringify(reponse));
      this.afficherMessge("reulst "+JSON.stringify(reponse))
    },error=>{
      this.afficherMessge("error"+JSON.stringify(error));
    })

  }

  ngOnInit() {
    this.addMettingForm=this.fb.group({
      subject:['',Validators.required],
      startTime:['',Validators.required],
      endTime:['',Validators.required],
    });
  }
  addMetting(){
    if(this.addMettingForm.valid){



      this.afficherMessge("  ******* month  reunion  start "+this.dateTimeLocal.toString());

      this.afficherMessge("  ******* month  reunion  end "+this.dateTimeLocalend.toString());



      let arrayStartSplit=this.dateTimeLocal.toString().split("-");
      let arrayEndSplit=this.dateTimeLocalend.toString().split("-");

      this.afficherMessge(" start year "+arrayStartSplit[0]+"    month "+arrayStartSplit[1]+" day "+arrayStartSplit[2]
        .substr(0,2) +" heure "+arrayStartSplit[2].substr(3,2)+" minute "+
        arrayStartSplit[2].substr(6,2))
      let reunion={
        subject:  this.addMettingForm.value["subject"],
        startTime:new Date(parseInt(arrayStartSplit[0]),parseInt(arrayStartSplit[1]),
          parseInt(arrayStartSplit[2].substr(0,2)) ,parseInt(arrayStartSplit[2].substr(3,2)),
          parseInt(arrayStartSplit[2].substr(6,2))
          ),
        endTime:new Date(parseInt(arrayEndSplit[0]),parseInt(arrayEndSplit[1]),
          parseInt(arrayEndSplit[2].substr(0,2)) ,parseInt(arrayEndSplit[2].substr(3,2)),
          parseInt(arrayEndSplit[2].substr(6,2))
        )
      }


      EndTime: new Date(2020, 6, 14, 14)

      this.reunionService.saveReunion(reunion,this.idCandiat).toPromise().then(reponse=>{
        this.afficherMessge("success"+JSON.stringify(reponse));
        Swal.fire({
          title: 'success adding  metting   ',
          text: 'success',
          icon: 'success',
        });
        this.router.navigate(["reunion"]);
      },error=>{
        Swal.fire({
          title: 'failure adding  metting   ',
          text: 'failure',
          icon: 'warning',
        });
        this.afficherMessge("failure")
      });

    }
    else{
      this.afficherMessge("not valid form");
    }
  }


  OnReset(){
    this.dialogRef.close();

  }
  candidatSelected($event: MatSelectChange) {
    this.idCandiat=$event.source.value;
    this.afficherMessge("id candiat "+$event.source.value)
  }




  afficherMessge(msg:any){
    console.log("**************add Meeting modal        "+msg);
  }

}
