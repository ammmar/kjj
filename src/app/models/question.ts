import { Option } from './option';

export class Question {
    id: number;
    name: string;
    //choix
    options: Option[];
    //seul choix
    questionTypeId: number;

  answered: boolean;

    constructor(data: any) {
        data = data || {};
        this.id = data.id;
        this.name = data.name;
        this.questionTypeId = data.questionTypeId;
        this.options = [];
        data.options.forEach(o => {
            this.options.push(new Option(o));
        });
    }
}
