import {Component ,OnInit, } from '@angular/core';

import {HttpClient} from '@angular/common/http';
import {  TemplateRef } from '@angular/core';

import {Observable} from "rxjs";
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {map, startWith} from "rxjs/operators";
import {MatAutocompleteSelectedEvent} from "@angular/material";
import { FormsModule } from '@angular/forms';
import {ActivatedRoute, Router} from "@angular/router";
import {MatSelectChange} from '@angular/material';
import Swal from "sweetalert2";
import {UserService} from '../../../services/user.service';
import {SharedService} from '../../../services/shared.service';
@Component({
  selector: 'app-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.scss']
})
export class ListUserComponent implements OnInit {
  public searchText:string;
  myControl = new FormControl();
  panelOpenState = false;
  listUsers:[]=[];
  constructor(private httpClient: HttpClient,private router:Router,private userService:UserService,private  sharedService:SharedService) {
    this.userService.getAllUsers().toPromise().then(reponse=>{
      this.afficheMsg("reponse"+JSON.stringify(reponse));
      this.listUsers=JSON.parse(JSON.stringify(reponse));
      this.afficheMsg("length array users"+this.listUsers.length);
    },error=>{
      this.afficheMsg("Error"+JSON.stringify(error));
    })
  }

  ngOnInit() {
  }


  NavToAdd(){
    this.router.navigate(["add-user"]);
  }



NavToUpdate(user:any){
    this.sharedService.changeMessage(user);
  this.router.navigate(["update-user"]);
}



  deleteUser(idUser:any)
  {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: 'Voulez vous supprimer ',
      text: "cette user",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Voulez vous supprimer!',
      cancelButtonText: 'Non!',
      reverseButtons: true
    }).then((result) => {

      if (result.value) {
        this.userService.deleteUser(idUser).toPromise().then(reponse=>{
          console.log("result de suprression"+JSON.stringify(reponse));
          swalWithBootstrapButtons.fire(
            'Supprimer!',
            'l operation a été effectué  .',
            'success')
        },error=>{
          swalWithBootstrapButtons.fire(
            'Probleme',
            'Probelem de suppression ',
            'warning')
        })




      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Anuller',
          'votre test est en cours  :)',
          'error'
        )
      }
    })
  }

  afficheMsg(msq:any){
    console.log("**************ListUserComponenet*****"+msq);
  }
}
