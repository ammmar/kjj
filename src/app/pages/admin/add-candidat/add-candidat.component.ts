import {Component ,OnInit, } from '@angular/core';

import {HttpClient} from '@angular/common/http';
import {  TemplateRef } from '@angular/core';

import {Observable} from "rxjs";
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {map, startWith} from "rxjs/operators";
import {MatAutocompleteSelectedEvent} from "@angular/material";
import { FormsModule } from '@angular/forms';
import {ActivatedRoute, Router} from "@angular/router";
import {MatSelectChange} from '@angular/material';
import Swal from "sweetalert2";
import {CandidatService} from '../../../services/candidat.service';

@Component({
  selector: 'app-add-candidat',
  templateUrl: './add-candidat.component.html',
  styleUrls: ['./add-candidat.component.scss']
})
export class AddCandidatComponent implements OnInit {
addCandidat:FormGroup;
  redirectDelay: number = 0;

  errors: string[] = [];
  messages: string[] = [];
  user: any = {rememberMe: true};
  today=new Date();
email:any;
  showMessages: any = {};
  submitted: boolean = false;
  currentMonth;

  validation = {};
  constructor(private  candidatService:CandidatService,private router:Router, private activatedRoute: ActivatedRoute,private  fb:FormBuilder) {
    let date: Date = new Date();


    let month : number = date.getMonth()+1;

    this.afficheMsq("current month "+this.getCurrentMonth(month)+"           date "+date);

  }


  getCurrentMonth(month:number):string
  {
    switch (month) {
      case 1:
      return "Janvier"
        console.log("It is a janvier.");
        break;
      case 2:
        return "Fevrier";
        console.log("It is a Fevrier.");
        break;
      case 3:
        return "Mars";
        console.log("It is a Tuesday.");
        break;
      case 4:
        return "Avril";
        console.log("It is a Wednesday.");
        break;
      case 5:
        return "Mai";
        console.log("It is a Mai.");
        break;
      case 6:
        return "Juin";

        console.log("It is a Juin.");
        break;
      case 7:
        return "Juillet";
        console.log("It is a Saturday.");
        break;
      case 8:
        return "aout";
        console.log("It is a Saturday.");
        break;
      case 9:
        return "septembre";
        console.log("It is a Saturday.");
        break;
      case 10:
        return "octobre";
        console.log("It is a Saturday.");
        break;
      case 11:
        return "novembre";
        console.log("It is a Saturday.");
        break;
      case 12:
        return "décembre";
        console.log("It is a Saturday.");
        break;
      default:
        console.log("No such day exists!");
        break;
    }
  }

  ngOnInit() {


    this.addCandidat=this.fb.group({
      nom:['',Validators.required],
      prenom:['',Validators.required],
      email:['',Validators.email],
      ecole:['',Validators.required],
      password:['',Validators.required],
      tel:['',Validators.required],
      experience:['',Validators.required],
      niveau:['',Validators.required],
      profil:['',Validators.required],
      dateDebot:['',Validators.required]
    });
  }

  onSubmit(){
    if(this.addCandidat.valid)
    {
      this.afficheMsq("Valid form "+this.addCandidat.value["nom"]);
      let date=new Date();
      let month : number = date.getMonth()+1;
      this.addCandidat.value["month"]=this.getCurrentMonth(month);
      this.candidatService.creerCandidat(this.addCandidat.value).toPromise().then(response=>{
        this.afficheMsq("add candiat "+JSON.stringify(response));
        Swal.fire({
          title: 'add candidat success  ',
          text: 'success',
          icon: 'success',
        });
        this.router.navigate(['/candidat/']);
      },error=>{
        Swal.fire({
          title: 'add candidat failure ',
          text: 'probleme d ajout candidat',
          icon: 'warning',
        });
        console.log("addTest:Erreur is =========>***"+JSON.stringify(error));
      });
    }
  }
  NavToList(){
    this.router.navigate(['/candidat/']);

  }

  afficheMsq(msg:any)
  {
    console.log("add-candiat      "+msg);
  }

}
