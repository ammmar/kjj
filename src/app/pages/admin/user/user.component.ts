import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import 'style-loader!angular2-toaster/toaster.css';
import {NbDialogService} from "@nebular/theme";
import {MatSelectChange} from '@angular/material';
import Swal from "sweetalert2";
import {UserService} from '../../../services/user.service';
@Component({
    selector: 'user-cmp',
    moduleId: module.id,
    templateUrl: 'user.component.html'
})

export class UserComponent implements OnInit{
  updateUser:FormGroup;
  user:any;
  id:any=188;
  today=new Date();
  username:any;
constructor(private  userService:UserService,private router:Router, private activatedRoute: ActivatedRoute,private  fb:FormBuilder) {
  this.userService.getUserById(this.id).toPromise().then(reponse=>{
    this.user=JSON.parse(JSON.stringify(reponse));
    this.username=this.user.username;
    this.afficheMessage("user is "+JSON.stringify(reponse));
  },error=>{
this.afficheMessage("error parsing user "+JSON.stringify(error));


  })
}
  ngOnInit(){
    this.updateUser=this.fb.group({
      username:['',Validators.required],
      password:['',Validators.required],
      firstname:['',Validators.required],
      lastname:['',Validators.required],
      email:['',Validators.required],
      usertel:['',Validators.required],
      useradresse:['',Validators.required]
    });

    }


  onSubmit(){
  if(this.updateUser.valid){
    this.updateUser.value["userdate"]=this.today;
    this.updateUser.value["lastPasswordResetDate"]=this.today;
    this.updateUser.value["disponibilite"]=true;
    this.updateUser.value["enabled"]=true;
    this.updateUser.value["poste"]="Admin";
    this.afficheMessage(" username"+this.updateUser.value["username"]);
    this.userService.updateUser(this.updateUser.value,this.id).toPromise().then(reponse=>{
      Swal.fire({
        title: 'update user success  ',
        text: 'success',
        icon: 'success',
      });
    },error=>{
      Swal.fire({
        title: 'update user failure  ',
        text: 'problem',
        icon: 'warning',
      });
    })
  }
  //valid version
  }




  afficheMessage(msg:any){
  console.log("*********** user ****"+msg);
  }
}
