import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {CandidatService} from '../../../services/candidat.service';
import {Observable} from 'rxjs';
import {FormControl} from '@angular/forms';
import Swal from "sweetalert2";
import {AffecterTestComponent} from '../../../Modal/affecter-test/affecter-test.component';
import {MatDialog} from '@angular/material/dialog';
import {AffecterCandidatTestComponent} from '../../../Modal/affecter-candidat-test/affecter-candidat-test.component';

@Component({
  selector: 'app-listcandidat',
  templateUrl: './listcandidat.component.html',
  styleUrls: ['./listcandidat.component.scss']
})
export class ListcandidatComponent implements OnInit {

  public searchText:string;
  myControl = new FormControl();
  panelOpenState = false;
  listCandiat:[]=[];

  bac:any="Bac +";

  constructor(private httpClient: HttpClient,private router:Router,private candidatService:CandidatService,public dialog: MatDialog) {
    this.candidatService.getAllCandidat().toPromise().then(response=>{
      this.afficheMessage("response"+JSON.stringify(response));
      this.listCandiat=JSON.parse(JSON.stringify(response));

      for(let i=0;this.listCandiat.length;i++){
        let q=this.listCandiat[i];
        this.afficheMessage("Candiat["+i+"]  nom"+q["nom"]+"ecole"+q["ecole"]);
      }
    },error=>{
      this.afficheMessage("************** error****====>"+JSON.stringify(error));
    })

  }

  openDialogAffecter(id_candidat:any,name:any,prenom:any): void {
    //this.afficheMsg("Dialog  id question"+id_question+" name question"+namequestion);
    const dialogRef = this.dialog.open(AffecterCandidatTestComponent, {
      width: '600px',
      height:'390px',
      data:{id_candidat:id_candidat,name:name,prenom:prenom}

    });
    console.log("AffecterDialog open");

    dialogRef.afterClosed().subscribe(res => {
      console.log("AffecterDialog closed");

    });
  }

  ngOnInit() {
  }
  NavToAdd(){
    this.router.navigate(['/add-candidat'])
  }
  NavToUpdate(id:any){

    this.afficheMessage("id"+id);
   // this.router.navigate(['/updateCandidat/',id])
    this.router.navigate(['/update-candidat',id]);

  }
  deleteCandidat(idCandidat:any){
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: 'Voulez vous supprimer ',
      text: "cette candidature Candidat",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Voulez vous supprimer!',
      cancelButtonText: 'Non!',
      reverseButtons: true
    }).then((result) => {

      if (result.value) {
       this.candidatService.deleteCandidat(idCandidat).toPromise().then(reponse=>{
         console.log("result de suprression"+JSON.stringify(reponse));
         swalWithBootstrapButtons.fire(
           'Supprimer!',
           'l operation a été effectué  .',
           'success')
       },error=>{
         swalWithBootstrapButtons.fire(
           'Probleme',
           'Probelem de suppression ',
           'warning')
       })




      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Anuller',
          'votre test est en cours  :)',
          'error'
        )
      }
    })
  }

  afficheMessage(msg:any){
    console.log("ListcandidatComponent"+msg);
  }
}
